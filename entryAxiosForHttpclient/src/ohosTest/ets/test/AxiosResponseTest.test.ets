/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import hilog from '@ohos.hilog';
import { describe, beforeAll, it, expect } from '@ohos/hypium'
import { axios, AxiosRequestConfig, AxiosResponse } from '@ohos/axiosforhttpclient'
import { NormalResultModel, PostDataModel } from '../../../main/ets/pages/types/types'
import { LOG, XTS_CONFIG } from '../../../main/ets/pages/common/Common'

export default function AxiosResponseTest() {
  let config: AxiosRequestConfig;

  describe('AxiosResponseTest', () => {
    const HTTP_COUNT = 2 //循环测试：测试http接口性能
    const TAG = LOG.TAG
    const DOMAIN = LOG.DOMAIN

    beforeAll(() => {
      config = {
        url: XTS_CONFIG.url,
        method: 'post',
        baseURL:  XTS_CONFIG.baseURL,
        headers: {
          "Content-Type": "application/json"
        },
        data: {
          newDate: 1,
          pageNum: 1,
          pageSize: 9
        },
        timeout: 1000,
      }
    })

    it('responseType_ARRAY_BUFFER', 1, async (done: Function) => {
      let startTime = new Date().getTime()
      let options: AxiosRequestConfig = JSON.parse(JSON.stringify(config))
      options.responseType = 'ARRAY_BUFFER'
      let res: AxiosResponse = await axios<NormalResultModel, AxiosResponse<NormalResultModel>, PostDataModel>(options)

      let endTime = new Date().getTime()
      let averageTime = (endTime - startTime) / HTTP_COUNT
      hilog.info(DOMAIN, TAG, " responseType_ARRAY_BUFFER averageTime: " + averageTime + ' μs');
      hilog.info(DOMAIN, TAG, " responseType_ARRAY_BUFFER averageTime: " + JSON.stringify(res.status));
      expect(res && res.data && (res.data instanceof ArrayBuffer))
        .assertTrue()
      expect(res && res.data && (typeof (res.data) === 'object') && !(res.data instanceof ArrayBuffer))
        .assertFalse()
      expect(res && res.data && (typeof (res.data) === 'string'))
        .assertFalse()
      done()
    })

    it('responseType_string', 2, async (done: Function) => {
      let startTime = new Date().getTime()
      let options: AxiosRequestConfig = JSON.parse(JSON.stringify(config))
      options.responseType = 'STRING'
      axios<NormalResultModel, AxiosResponse<NormalResultModel>, PostDataModel>(options).then((res: AxiosResponse<NormalResultModel>) => {
        let endTime = new Date().getTime()
        let averageTime = (endTime - startTime) / HTTP_COUNT
        hilog.info(DOMAIN, TAG, " responseType_string averageTime: " + averageTime + ' μs');
        hilog.info(DOMAIN, TAG, " responseType_string averageTime: " + JSON.stringify(res.status));
        expect(res && res.data && (res.data instanceof ArrayBuffer))
          .assertFalse()
        expect(res && res.data && (typeof (res.data) === 'object') && !(res.data instanceof ArrayBuffer))
          .assertFalse()
        expect(res && res.data && (typeof (res.data) === 'string'))
          .assertTrue()
        done()
      })
    })

    it('responseType_OBJECT', 3, async (done: Function) => {
      let startTime = new Date().getTime()
      let options: AxiosRequestConfig = JSON.parse(JSON.stringify(config))
      options.responseType = 'OBJECT'
      axios<NormalResultModel, AxiosResponse<NormalResultModel>, PostDataModel>(options).then((res: AxiosResponse<NormalResultModel>) => {
        let endTime = new Date().getTime()
        let averageTime = (endTime - startTime) / HTTP_COUNT
        hilog.info(DOMAIN, TAG, " responseType_OBJECT averageTime: " + averageTime + ' μs');
        hilog.info(DOMAIN, TAG, " responseType_OBJECT averageTime: " + JSON.stringify(res.status));
        expect(res.data instanceof ArrayBuffer)
          .assertFalse()
        expect(res && res.data && (typeof (res.data) === 'object') && !(res.data instanceof ArrayBuffer))
          .assertTrue()
        expect(res && res.data && (typeof (res.data) === 'string'))
          .assertFalse()
        done()
      })
    })
  })
}
