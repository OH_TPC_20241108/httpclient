/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the 'License');
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an 'AS IS' BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import hilog from '@ohos.hilog';
import { describe, it, expect } from '@ohos/hypium'
import { axios, AxiosRequestConfig, AxiosResponse, AxiosError } from '@ohos/axiosforhttpclient'
import { InfoModel } from '../../../main/ets/pages/types/types'
import fs from '@ohos.file.fs';
import { GlobalContext } from '../testability/GlobalContext';
import { LOG, XTS_CONFIG } from '../../../main/ets/pages/common/Common'
import { writeFile, readFile } from '../../../main/ets/pages/common/fileFs'

export default function abilityTest() {

  describe('AxiosCertificate', () => {
    const TAG = LOG.TAG;
    const DOMAIN = LOG.DOMAIN;
    const pathOneWayAuth = XTS_CONFIG.pathOneWayAuth; // 单向校验请求路径
    const pathMutualAuthNoPassword = XTS_CONFIG.pathMutualAuthNoPassword; // 双向校验无密码请求路径
    const pathMutualAuthHasPassword = XTS_CONFIG.pathMutualAuthHasPassword; // 双向校验有密码请求路径
    const pathCertificatePin = XTS_CONFIG.pathCertificatePin; //证书锁定的请求路径
    const pwd = XTS_CONFIG.psw

    //单向认证，p12格式证书
    // it('caPath_p12', 0, async (done: Function) => {
    //   let filePath: string  = ''
    //   let context: Context = GlobalContext.getContext().getObject("context") as Context;
    //   let filesDir: string = context.filesDir;
    //
    //   //获取根证书路径
    //   try {
    //     let ca: Uint8Array = await context.resourceManager.getRawFileContent("oneWayAuth/ca.p12");
    //     if (ca != null) {
    //       filePath = filesDir + "/ca.p12";
    //       let file = fs.openSync(filePath, fs.OpenMode.READ_WRITE | fs.OpenMode.CREATE);
    //       fs.writeSync(file.fd, ca.buffer);
    //       fs.fsyncSync(file.fd);
    //       fs.closeSync(file);
    //     }
    //
    //     // 发送请求
    //     let startTime = new Date().getTime()
    //     axios<InfoModel, AxiosResponse<InfoModel>, null>({
    //       url: pathOneWayAuth,
    //       method: 'get',
    //       caPath: filePath,
    //     }).then((res: AxiosResponse<InfoModel>) => {
    //       let endTime = new Date().getTime();
    //       let averageTime = endTime - startTime;
    //       hilog.info(DOMAIN, TAG, " caPath_p12 averageTime: " + averageTime + ' μs');
    //       hilog.info(DOMAIN, TAG, " caPath_p12 averageTime: " + JSON.stringify(res.status));
    //       expect(res && res.status && res.status === 200)
    //         .assertTrue()
    //       done()
    //     })
    //   }
    //   catch (err) {
    //     expect().assertFail()
    //   }
    // })

    //单向认证，pem格式证书  PYNNN::单向证书有异常
    it('caPath_pem', 1, async (done: Function) => {
      let filePath: string  = 'oneWayAuth/ca.pem';
      let context: Context = GlobalContext.getContext().getObject("context") as Context;
      // let filesDir: string = context.filesDir;

      //获取根证书路径
      try {
        // 发送请求
        let startTime = new Date().getTime()
        axios<InfoModel, AxiosResponse<InfoModel>, null>({
          url: pathOneWayAuth,
          method: 'get',
          caPath: filePath,
          context: context,
        }).then((res: AxiosResponse<InfoModel>) => {
          let endTime = new Date().getTime()
          let averageTime = endTime - startTime;
          hilog.info(DOMAIN, TAG, " caPath_pem averageTime: " + averageTime + ' μs');
          hilog.info(DOMAIN, TAG, " caPath_pem averageTime: " + JSON.stringify(res.status));
          expect(res && res.status && res.status === 200)
            .assertTrue()
          done()
        })
      }
      catch (err) {
        expect().assertFail()
      }
    })

    //双向验证，无密码，p12格式证书
    // it('clientCert_noPsw_p12', 2, async (done: Function) => {
    //   let path_ca = '' // 根证书路径
    //   let path_client = '' // 客户端证书路径
    //   let path_key = '' // 客户端密码路径
    //
    //   let context: Context = GlobalContext.getContext().getObject("context") as Context;
    //   let filesDir: string = context.filesDir;
    //
    //   //获取根证书路径
    //   try {
    //     let ca: Uint8Array = await context.resourceManager.getRawFileContent("mutualAuth_noPsw/ca.crt");
    //     let client: Uint8Array = await context.resourceManager.getRawFileContent("mutualAuth_noPsw/p12/client.p12");
    //     let key: Uint8Array = await context.resourceManager.getRawFileContent("mutualAuth_noPsw/p12/client.key");
    //
    //     if (ca != null) {
    //       path_ca = filesDir + "/ca.crt";
    //       writeFile(filesDir, 'ca.crt', ca.buffer)
    //     }
    //     if (client != null) {
    //       path_client = filesDir + "/client.p12";
    //       writeFile(filesDir, 'client.p12', client.buffer)
    //     }
    //     if (key != null) {
    //       path_key = filesDir + "/client.key";
    //       writeFile(filesDir, 'client.key', key.buffer)
    //     }
    //
    //     // 发送请求
    //     let startTime = new Date().getTime()
    //     axios<InfoModel, AxiosResponse<InfoModel>, null>({
    //       url: pathMutualAuthNoPassword,
    //       method: 'get',
    //       caPath: path_ca,
    //       clientCert: {
    //         certPath: path_client,
    //         certType: 'p12',
    //         keyPath: path_key,
    //       }
    //     }).then((res: AxiosResponse<InfoModel>) => {
    //       let endTime = new Date().getTime()
    //       let averageTime = endTime - startTime
    //       hilog.info(DOMAIN, TAG, " clientCert_noPsw_p12 averageTime: " + averageTime + ' μs');
    //       hilog.info(DOMAIN, TAG, " clientCert_noPsw_p12 averageTime: " + JSON.stringify(res.status));
    //       expect(res && res.status && res.status === 200)
    //         .assertTrue()
    //       done()
    //     })
    //   }
    //   catch (err) {
    //     expect().assertFail()
    //   }
    // })

    //双向验证，无密码，pem格式证书
    it('clientCert_noPsw_pem', 3, async (done: Function) => {
      let path_ca = 'mutualAuth_noPsw/ca.crt'; // 根证书路径
      let path_client = 'mutualAuth_noPsw/pem/client.pem'; // 客户端证书路径
      let path_key = 'mutualAuth_noPsw/pem/client.key'; // 客户端密码路径

      let context: Context = GlobalContext.getContext().getObject("context") as Context;
      let filesDir: string = context.filesDir;

      //获取根证书路径
      try {
        // 发送请求
        let startTime = new Date().getTime()
        axios<InfoModel, AxiosResponse<InfoModel>, null>({
          url: pathMutualAuthNoPassword,
          method: 'get',
          caPath: path_ca,
          clientCert: {
            certPath: path_client,
            certType: 'pem',
            keyPath: path_key,
          },
          context: context,
        }).then((res: AxiosResponse<InfoModel>) => {
          let endTime = new Date().getTime();
          let averageTime = endTime - startTime;
          hilog.info(DOMAIN, TAG, " clientCert_noPsw_pem averageTime: " + averageTime + ' μs');
          hilog.info(DOMAIN, TAG, " clientCert_noPsw_pem averageTime: " + JSON.stringify(res.status));
          expect(res && res.status && res.status === 200)
            .assertTrue()
          done()
        })
      }
      catch (err) {
        expect().assertFail();
      }
    })

    //双向验证，有密码，p12格式证书
    // it('clientCert_hasPsw_p12', 4, async (done: Function) => {
    //   let path_ca = ''; // 根证书路径
    //   let path_client = ''; // 客户端证书路径
    //
    //   let context: Context = GlobalContext.getContext().getObject("context") as Context;
    //   let filesDir: string = context.filesDir;
    //
    //   //获取根证书路径
    //   try {
    //     let ca: Uint8Array = await context.resourceManager.getRawFileContent("mutualAuth_hasPsw/ca.crt");
    //     let client: Uint8Array = await context.resourceManager.getRawFileContent("mutualAuth_hasPsw/p12/client.p12");
    //
    //     if (ca != null) {
    //       path_ca = filesDir + "/ca.crt";
    //       writeFile(filesDir, 'ca.crt', ca.buffer)
    //     }
    //     if (client != null) {
    //       path_client = filesDir + "/client.p12";
    //       writeFile(filesDir, 'client.p12', client.buffer)
    //     }
    //
    //     // 发送请求
    //     let startTime = new Date().getTime()
    //     axios<InfoModel, AxiosResponse<InfoModel>, null>({
    //       url: pathMutualAuthHasPassword,
    //       method: 'get',
    //       caPath: path_ca,
    //       clientCert: {
    //         certPath: path_client,
    //         certType: 'p12',
    //         keyPath: '',
    //         keyPasswd: pwd
    //       }
    //     }).then((res: AxiosResponse<InfoModel>) => {
    //       let endTime = new Date().getTime()
    //       let averageTime = endTime - startTime
    //       hilog.info(DOMAIN, TAG, " clientCert_hasPsw_p12 averageTime: " + averageTime + ' μs');
    //       hilog.info(DOMAIN, TAG, " clientCert_hasPsw_p12 averageTime: " + JSON.stringify(res.status));
    //       expect(res && res.status && res.status === 200)
    //         .assertTrue()
    //       done()
    //     })
    //   }
    //   catch (err) {
    //     expect().assertFail()
    //   }
    // })

    //双向验证，有密码，pem格式证书
    it('clientCert_hasPsw_pem', 5, async (done: Function) => {
      let path_ca = 'mutualAuth_hasPsw/ca.crt'; // 根证书路径
      let path_client = 'mutualAuth_hasPsw/pem/client.pem'; // 客户端证书路径
      let path_key = 'mutualAuth_hasPsw/pem/client.key'; // 客户端密码路径

      let context: Context = GlobalContext.getContext().getObject("context") as Context;
      let filesDir: string = context.filesDir;

      //获取根证书路径
      try {
        // 发送请求
        let startTime = new Date().getTime()
        axios<InfoModel, AxiosResponse<InfoModel>, null>({
          url: pathMutualAuthHasPassword,
          method: 'get',
          caPath: path_ca,
          clientCert: {
            certPath: path_client,
            certType: 'pem',
            keyPath: path_key,
            keyPasswd: pwd
          },
          context: context,
        }).then((res: AxiosResponse<InfoModel>) => {
          let endTime = new Date().getTime()
          let averageTime = endTime - startTime
          hilog.info(DOMAIN, TAG, " clientCert_hasPsw_pem averageTime: " + averageTime + ' μs');
          hilog.info(DOMAIN, TAG, " clientCert_hasPsw_pem averageTime: " + JSON.stringify(res.status));
          expect(res && res.status && res.status === 200)
            .assertTrue()
          done()
        })
      }
      catch (err) {
        expect().assertFail()
      }
    })
  })
}