## 1.0.0
1. 发布正式版1.0.0

## 1.0.0-rc.6

1. axiosForHttpclient组件的依赖库httpclient升级到2.0.2-rc.0版本。

## 1.0.0-rc.5

1. axiosForHttpclient组件的依赖库axios适配升级到2.2.2版本。

## 1.0.0-rc.4

1. httpclient修复数据大时，字符串处理异常的问题。
2. httpclient修复post请求时，部分接口响应的result结果为空问题。
3. httpclient新增Logger开关方法setDebugSwitch。

## 1.0.0-rc.3

1. 支持系统默认的CA证书进行通信

## 1.0.0-rc.2

1. 修复通过axios方法返回的headers类型，使返回数据中的headers按照正常格式解析

## 1.0.0-rc.1

1. httpclient适配axios功能,支持axios的上传和下载,支持通过axios配置直接调用httpclient的功能
