/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {
  Dns,
  HttpClient,
  Request,
  Response,
  TimeUnit,
  CertificatePinnerBuilder,
  Logger,
  Utils
} from '@ohos/httpclient';
import { Utils as GetCAUtils } from "../utils/Utils"
import connection from '@ohos.net.connection';
import { BusinessError } from '@ohos.base';
import { CertificatePinner } from '@ohos/httpclient/src/main/ets/CertificatePinner';
import { DEMO_CONFIG } from '../common/Common';

@Entry
@Component
struct certificatePinner {
  @State url: string | undefined = DEMO_CONFIG.httpsServer+DEMO_CONFIG.getUrl;
  @State result: string | undefined = this.getResourceString($r('app.string.Response_results'));
  @State hostname: string | undefined = DEMO_CONFIG.ip;
  @State successPin: string | undefined = DEMO_CONFIG.httpsPin;
  @State failPin: string | undefined = 'sha1/f58c753eff28a6c';
  ca: string =  DEMO_CONFIG.noPasswordCa

  getResourceString(res: Resource) {
    return getContext().resourceManager.getStringSync(res.id)
  }

  build() {
    Column() {
      Row() {
        Navigator({
          target: '',
          type: NavigationType.Back
        }) {
          Text($r('app.string.BACK'))
            .fontSize(10)
            .border({
              width: 1
            })
            .padding(10)
            .fontColor(0x000000)
            .borderColor(0x317aff)
        }
      }
      .height('10%')
      .width('100%')

      TextInput({ text: this.url, placeholder: $r('app.string.Please_URL') })
        .placeholderColor('#ffffff')
        .caretColor(Color.Blue)
        .height('150px')
        .fontSize('18fp')
        .onChange((value: string) => {
          this.url = value
        })
        .margin(10)

      TextInput({ text: this.hostname, placeholder: $r('app.string.Please_hostname') })
        .placeholderColor('#ffffff')
        .caretColor(Color.Blue)
        .height('150px')
        .fontSize('18fp')
        .onChange((value: string) => {
          this.hostname = value
        })
        .margin(10)

      TextInput({ text: this.successPin, placeholder: $r('app.string.Please_fingerprint') })
        .placeholderColor('#ffffff')
        .caretColor(Color.Blue)
        .height('150px')
        .fontSize('18fp')
        .onChange((value: string) => {
          this.successPin = value
        })
        .margin(10)

      TextInput({ text: this.failPin, placeholder: $r('app.string.Please_failed_fingerprint') })
        .placeholderColor('#ffffff')
        .caretColor(Color.Blue)
        .height('150px')
        .fontSize('18fp')
        .onChange((value: string) => {
          this.failPin = value
        })
        .margin(10)

      Button($r('app.string.Initiate_verification'))
        .width('80%')
        .height('80px')
        .fontSize(18)
        .fontColor(0xCCCCCC)
        .align(Alignment.Center)
        .margin(10)
        .onClick(async () => {
          let CA: string = await new GetCAUtils().getCA(this.ca, getContext());
          let certificatePinner: CertificatePinner | undefined = new CertificatePinnerBuilder()
            .add(this.hostname, this.successPin)
            .build()
          let client: HttpClient | undefined = new HttpClient
            .Builder()
            .dns(new CustomDns())
            .setConnectTimeout(10, TimeUnit.SECONDS)
            .setReadTimeout(10, TimeUnit.SECONDS)
            .build();
          let request: Request | undefined = new Request.Builder()
            .url(this.url)
            .method('GET')
            .ca([CA])
            .build();
          client.newCall(request)
            .setCertificatePinner(certificatePinner)
            .enqueue((result: Response) => {
              this.result = this.getResourceString($r('app.string.Response_result_success')) + JSON.stringify(JSON.parse(JSON.stringify(result)), null, 4)
              Logger.info('证书锁定---success---' + JSON.stringify(JSON.parse(JSON.stringify(result)), null, 4));
              certificatePinner = undefined
              client = undefined
              request = undefined
            }, (err: BusinessError) => {
              this.result = this.getResourceString($r('app.string.Response_failed')) + JSON.stringify(err)
              Logger.info('证书锁定---failed--- ', JSON.stringify(err));
              certificatePinner = undefined
              client = undefined
              request = undefined
            });
        })

      Button($r('app.string.Initiate_failed'))
        .width('80%')
        .height('80px')
        .fontSize(18)
        .fontColor(0xCCCCCC)
        .align(Alignment.Center)
        .margin(10)
        .onClick(async () => {
          let CA: string = await new GetCAUtils().getCA(this.ca, getContext());
          let certificatePinner: CertificatePinner | undefined = new CertificatePinnerBuilder()
            .add(this.hostname, this.failPin)
            .build();
          let client: HttpClient | undefined = new HttpClient
            .Builder()
            .dns(new CustomDns())
            .setConnectTimeout(10, TimeUnit.SECONDS)
            .setReadTimeout(10, TimeUnit.SECONDS)
            .build();
          let request: Request | undefined = new Request.Builder()
            .url(this.url)
            .method('GET')
            .ca([CA])
            .build();
          client.newCall(request)
            .setCertificatePinner(certificatePinner)
            .enqueue((result: Response) => {
              this.result = this.getResourceString($r('app.string.Response_result_success')) + JSON.stringify(JSON.parse(JSON.stringify(result)), null, 4);
              Logger.info('证书锁定---success---' + JSON.stringify(JSON.parse(JSON.stringify(result)), null, 4));
              certificatePinner = undefined
              client = undefined
              request = undefined
            }, (err: BusinessError) => {
              this.result = this.getResourceString($r('app.string.Response_failed')) + JSON.stringify(err);
              Logger.info('证书锁定---failed--- ', JSON.stringify(err));
              certificatePinner = undefined
              client = undefined
              request = undefined
            });
        })

      Scroll() {
        Column() {
          Text(this.result)
            .width('80%')
            .fontSize("18fp")
            .margin(10);
        };
      }
      .width('100%')
      .layoutWeight(1);
    }
    .width('100%')
    .height('100%');
  };

  aboutToDisappear(): void {
    this.url = undefined
    this.result = undefined
    this.hostname = undefined
    this.successPin = undefined
    this.failPin = undefined
  }

  Uint8ArrayToString(fileData: Uint8Array): string {
    let dataString = '';
    for (let i = 0; i < fileData.length; i++) {
      dataString += String.fromCharCode(fileData[i]);
    }
    return dataString;
  };
}


export class CustomDns implements Dns {
  async lookup(hostname: string): Promise<Array<connection.NetAddress>> {
    console.info('DNSTEST CustomDns begin here');
    return await new Promise((resolve, reject) => {
      let netAddress: Array<connection.NetAddress> = [{ 'address': DEMO_CONFIG.ip, 'family': 1, 'port': 8001 }];
      ;
      resolve(netAddress);
    })
  };
}