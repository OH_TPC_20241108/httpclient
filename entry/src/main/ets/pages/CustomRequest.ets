/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Author from '../model/author';
import { HttpClient, Request, RequestBody, TimeUnit } from '@ohos/httpclient';
import Weather from '../model/Weather';
import hilog from '@ohos.hilog';
import { BusinessError } from '@ohos.base';

@Entry
@Component
struct CustomRequest {
  scroller: Scroller = new Scroller();
  @State content: string = "";
  @State result: string = this.getResourceString($r('app.string.Request_result')) + "\r\n"
  @State Country: string = this.getResourceString($r('app.string.Attribute_values')) + "\r\n"
  @State Isp: string = this.getResourceString($r('app.string.Attribute_values')) + "\r\n"
  @State result_queue: string = this.getResourceString($r('app.string.Request_result')) + "\r\n"
  @State property_two_queue: string = this.getResourceString($r('app.string.Attribute_values')) + "\r\n"
  @State property_one_queue: string = this.getResourceString($r('app.string.Attribute_values')) + "\r\n"
  client: HttpClient = new HttpClient
    .Builder()
    .setConnectTimeout(10, TimeUnit.SECONDS)
    .setReadTimeout(10, TimeUnit.SECONDS)
    .build();

  getResourceString(res: Resource) {
    return getContext().resourceManager.getStringSync(res.id)
  }

  build() {
    Column() {
      Flex({
        direction: FlexDirection.Column
      }) {
        Navigator({
          target: "",
          type: NavigationType.Back
        }) {
          Text($r('app.string.BACK'))
            .fontSize(10)
            .border({
              width: 1
            })
            .padding(10)
            .fontColor(0x000000)
            .borderColor(0x317aff)
        }
      }
      .height('10%')
      .width('100%')
      .padding(10)

      Text(this.getResourceString($r('app.string.What_object')) + '\r\n')
        .fontSize(18)
        .fontColor(0xCCCCCC)
        .align(Alignment.Center)
        .margin(30)
      Scroll(this.scroller) {
        Column() {
          Button($r('app.string.Synchronize_requests'))
            .width('80%')
            .height('80px')
            .fontSize(18)
            .fontColor(0xCCCCCC)
            .align(Alignment.Center)
            .margin(30)
            .onClick((event: ClickEvent) => {
              let authorId = "abc7689"
              let userName = "TestUser";
              let emailId = "testmail@gmail.com";
              let password = "password123";
              let author = new Author(authorId, userName, emailId, password);

              let requestBody: RequestBody = RequestBody.create(JSON.stringify(author))
              let request: Request = new Request.Builder()
                .url("http://apis.juhe.cn/simpleWeather/query?city=%E8%8B%8F%E5%B7%9E&key=b01495997ccfd574b61f2976dadd76df")
                .post()
                .body(requestBody)
                .addHeader("Content-Type", "application/json")
                .setEntryObj(new Weather())
                .build();

              this.client.newCall(request)
                .executed()
                .then((result: Response) => { // 返回的实体对象
                  this.result = this.getResourceString($r('app.string.Request_result')) + "\r\n" + JSON.stringify(result)
                  console.log('Custom Request Results 222 == ' + JSON.stringify(result))
                  // 调用对象的Isp属性
                  this.Country = this.getResourceString($r('app.string.Attribute_values')) + "\r\n" + JSON.stringify(result.result.future);
                  console.log('Custom Request Object Properties == ' + result.result.future)
                  this.Isp = this.getResourceString($r('app.string.Attribute_values')) + "\r\n" + result.result.city;
                  console.log('Custom Request Object Properties == ' + result.result.city)
                })
                .catch((err: BusinessError<string>) => {
                  if (err.message != undefined) {
                    this.content = err.message;
                  }
                });
            })

          Text(this.result)
            .width('80%')
            .fontSize("18fp")
            .margin(20)
          Text(this.Country)
            .width('80%')
            .fontSize("18fp")
            .margin(10)
          Text(this.Isp)
            .width('80%')
            .fontSize("18fp")
            .margin(10)

          Button($r('app.string.Asynchronous_request'))
            .width('80%')
            .height('80px')
            .fontSize(18)
            .fontColor(0xCCCCCC)
            .align(Alignment.Center)
            .margin(30)
            .onClick((event: ClickEvent) => {
              let authorId = "abc7689"
              let userName = "TestUser";
              let emailId = "testmail@gmail.com";
              let password = "password123";
              let author = new Author(authorId, userName, emailId, password);

              let requestBody: RequestBody = RequestBody.create(JSON.stringify(author))
              let request: Request = new Request.Builder()
                .url("http://apis.juhe.cn/simpleWeather/query?city=%E8%8B%8F%E5%B7%9E&key=b01495997ccfd574b61f2976dadd76df")
                .post()
                .body(requestBody)
                .addHeader("Content-Type", "application/json")
                .setEntryObj(new Weather(), true)//设置自定义请求的实体对象,第二个参数为是否自定义请求（默认不是，false）
                .build();
              this.client.newCall(request)
                .enqueue(
                  (result: Response) => {
                    console.log('Custom Request Results == ' + JSON.stringify(result))
                    this.result_queue = this.getResourceString($r('app.string.Request_result')) + "\r\n" + JSON.stringify(result)
                    this.property_one_queue = this.getResourceString($r('app.string.Attribute_values')) + "\r\n" + JSON.stringify(result.result.future);
                    console.log('Custom Request Object Properties == ' + JSON.stringify(result.result.future))
                    this.property_two_queue = this.getResourceString($r('app.string.Attribute_values')) + "\r\n" + result.result.city;
                    console.log('Custom Request Object Properties == ' + result.result.city)
                  }, (error: BusinessError) => {
                  this.result_queue = this.getResourceString($r('app.string.Request_result')) +"\r\n" + JSON.stringify(error);
                  hilog.info(0x0001, "Custom Request error == ", JSON.stringify(error));
                })
            })

          Text(this.result_queue)
            .width('80%')
            .fontSize("18fp")
            .margin(20)
          Text(this.property_one_queue)
            .width('80%')
            .fontSize("18fp")
            .margin(10)
          Text(this.property_two_queue)
            .width('80%')
            .fontSize("18fp")
            .margin(10)
        }
      }
      .layoutWeight(1)
      .scrollBar(BarState.Off)
      .scrollable(ScrollDirection.Vertical)
    }
    .width('100%')
  }
}

interface Response {
  reason: string;
  result: Result;
  error_code: number;
}

interface Result {
  city: string;
  realtime: RealTimeType;
  future: FutureType;
}

interface RealTimeType {
  temperature: string;
  humidity: string;
  info: string;
  wid: string;
  direct: string;
  power: string;
  aqi: string;
}

interface FutureType {
  date: string;
  temperature: string;
  wid: WidType;
  direct: string;
}

interface WidType {
  day: string;
  night: string;
}