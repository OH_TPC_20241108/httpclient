/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { FileUpload, gZipUtil, HttpClient, Mime, Request, RequestBody, Response } from '@ohos/httpclient'
import fs from '@ohos.file.fs';
import Log from '../model/log'
import util from '@ohos.util';
import buffer from '@ohos.buffer';
import hilog from '@ohos.hilog';
import resmgr from '@ohos.resourceManager';
import { BusinessError } from '@ohos/httpclient/src/main/ets/http';

@Entry
@Component
struct Compression {
  @State status: string = '';
  @State content: string = '';
  downloadfile: string = "";
  fileName: string = "/gziptest.txt";
  client: HttpClient = new HttpClient.Builder().setConnectTimeout(10000).build();
  scroller: Scroller = new Scroller()
  ipInput: string = '106.15.92.248'
  portInput: string = '9090'
  baseServer = this.getUrl()
  hereAbilityContext: Context = getContext();
  hereCacheDir: string = this.hereAbilityContext.cacheDir;
  hereFilesDir: string = this.hereAbilityContext.filesDir;

  getResourceString(res: Resource) {
    return getContext().resourceManager.getStringSync(res.id)
  }

  build() {
    Column() {
      Flex({ direction: FlexDirection.Column }) {
        Navigator({ target: "", type: NavigationType.Back }) {
          Text('BACK')
            .fontSize(12)
            .border({ width: 1 })
            .padding(10)
            .fontColor(0x000000)
            .borderColor(0x317aff)
        }
      }
      .height('10%')
      .width('100%')
      .padding(10)

      Row() {
        TextInput({ text: this.ipInput, placeholder: $r('app.string.Enter_server_IP') })
          .layoutWeight(1)
          .height('100%')
          .fontSize(18)
          .onChange((value: string) => {
            this.ipInput = value
          })
        Blank().width(px2vp(5))
        TextInput({ text: this.portInput, placeholder: $r('app.string.Enter_port') })
          .layoutWeight(1)
          .height('100%')
          .fontSize(18)
          .onChange((value: string) => {
            this.portInput = value
          })
      }.height(px2vp(150))

      Row() {
        Button('GZIP Encode')
          .layoutWeight(1)
          .fontSize(18)
          .fontColor(0xCCCCCC)
          .align(Alignment.Center)
          .height('100%')
          .onClick((event: ClickEvent) => {
            const test = "hello, GZIP! this is a gzip word";

            let compressed: Uint8Array = gZipUtil.gZipString(test);
            this.status = this.getResourceString($r('app.string.Encoding_successful'));
            this.content = this.getResourceString($r('app.string.Encoding_data')) + JSON.stringify(compressed);
            Log.showInfo("gzip Encode after gzip and compressed result is " + compressed);
          })

        Button('GZIP Decode')
          .layoutWeight(1)
          .fontSize(18)
          .fontColor(0xCCCCCC)
          .align(Alignment.Center)
          .height('100%')
          .margin({ left: 5 })
          .onClick((event: ClickEvent) => {
            const test = "hello, GZIP! this is a gzip word";
            let compressed: Uint8Array = gZipUtil.gZipString(test);

            Log.showInfo("gzipencodeTest after gzip and compressed result is " + compressed);

            let restored: string = gZipUtil.ungZipString(JSON.parse(JSON.stringify(compressed)));

            this.status = this.getResourceString($r('app.string.Decoding_successful'));
            this.content = this.getResourceString($r('app.string.Decoded_data')) + restored
            // Output to console
            Log.showInfo("gzipdecodeTest and uncompressed result is " + JSON.stringify(restored));
          })
      }.height(px2vp(60)).margin({ top: 10 })


      Row() {
        Button('GZIP Encode File')
          .layoutWeight(1)
          .fontSize(18)
          .fontColor(0xCCCCCC)
          .align(Alignment.Center)
          .height('100%')
          .onClick((event: ClickEvent) => {
            Log.showInfo("gzipEncodeFileTest and appInternalDir is " + this.hereCacheDir);

            let encodeStr = "hello, GZIP! this is a gzip word"

            let resourcePath = this.hereCacheDir + "/hello.txt";
            let gzipPath = this.hereCacheDir + "/test.txt.gz";

            Log.showInfo("gzipEncodeFile test path:dest" + gzipPath);
            this.gzipStrAndSaveFile(encodeStr, resourcePath, gzipPath);

            Log.showInfo("gzipEncodeFile test success");
            this.content = '\n' + this.getResourceString($r('app.string.Encoding_data')) + encodeStr + '\n'
            this.content += '\n' + this.getResourceString($r('app.string.Encoding_path')) + gzipPath + '\n'
            this.status = this.getResourceString($r('app.string.Encoding_successful'));
          })


        Button('GZIP Decode File')
          .layoutWeight(1)
          .fontSize(18)
          .fontColor(0xCCCCCC)
          .align(Alignment.Center)
          .height('100%')
          .margin({ left: 5 })
          .onClick(async (event: ClickEvent) => {
            let encodeStr = "hello, GZIP! this is a gzip word"

            let resourcePath = this.hereCacheDir + "/hello.txt";
            let gzipPath = this.hereCacheDir + "/test.txt.gz";
            this.gzipStrAndSaveFile(encodeStr, resourcePath, gzipPath);

            Log.showInfo("gzipDecodeFiletest and  appInternalDir is " + this.hereCacheDir);

            let dest = this.hereCacheDir + "/hello2.txt";

            await gZipUtil.ungZipFile(gzipPath, dest);
            Log.showInfo("gzipDecodeFiletest success");


            let fileID = fs.openSync(dest, fs.OpenMode.READ_WRITE | fs.OpenMode.CREATE)
            // 获取文件信息
            let stat = fs.statSync(fileID.fd);
            let size = stat.size // 文件的大小，以字节为单位
            let buf = new ArrayBuffer(size);
            fs.readSync(fileID.fd, buf)
            let textDecoder = new util.TextDecoder("utf-8", { ignoreBOM: true });
            let decodedString = textDecoder.decode(new Uint8Array(buf), { stream: false });
            this.status = this.getResourceString($r('app.string.Decoding_successful'));
            this.content = '\n' + this.getResourceString($r('app.string.Original_path')) + gzipPath + '\n'
            this.content += '\n' + this.getResourceString($r('app.string.Decoding_path')) + dest + '\n'
            this.content += '\n' + this.getResourceString($r('app.string.File_size')) + size + ' byte' + '\n'
            this.content += '\n' + this.getResourceString($r('app.string.Decoding_result')) + decodedString + '\n'

          })

      }.margin({ top: 10 }).height(px2vp(60))

      Row() {
        Button('GZIP Upload File')
          .layoutWeight(1)
          .fontSize(18)
          .fontColor(0xCCCCCC)
          .align(Alignment.Center)
          .height('100%')
          .onClick((event: ClickEvent) => {
            this.status = this.getResourceString($r('app.string.Uploading'));
            this.content = ''
            Log.showInfo(" cacheDir   " + this.hereCacheDir)

            let filePath = this.hereCacheDir + "/hello.txt";
            let fd = fs.openSync(filePath, fs.OpenMode.CREATE | fs.OpenMode.READ_WRITE);
            fs.truncateSync(fd.fd);
            fs.writeSync(fd.fd, "hello, world! hello HttpClientServer hello httpclientServer");

            fs.fsyncSync(fd.fd);
            fs.closeSync(fd);

            let destpath = this.hereCacheDir + "/test2.txt.gz";

            gZipUtil.gZipFile(filePath, destpath);
            Log.showInfo("gZipFile file success   ")

            destpath = destpath.replace(this.hereCacheDir, "internal://cache");

            let fileUploadBuilder: FileUpload = new FileUpload.Builder()
              .addFile(destpath)
              .addData("filename", "test2.txt")
              .build();
            let fileObject: object[] = fileUploadBuilder.getFile();
            let dataObject: object[] = fileUploadBuilder.getData();
            Log.showInfo("fileObject:   " + JSON.stringify(fileObject));
            Log.showInfo("dataObject:   " + JSON.stringify(dataObject));

            Log.showInfo('about to set : abilityContext - cacheDir  = ' + this.hereCacheDir);
            Log.showInfo('about to Set : abilityContext - filesDir  = ' + this.hereFilesDir);
            Log.showInfo("type of :" + typeof this.hereAbilityContext)

            let request: Request = new Request.Builder()
              .url(this.getUrl() + '/uploadGzipFiles')
              .addFileParams(fileObject, dataObject)
              .setAbilityContext(this.hereAbilityContext)
              .build();
            this.client.newCall(request)
              .execute()
              .then((data: Response) => {
                data.uploadTask.on('progress', (uploadedSize: number, totalSize: number) => {
                  Log.showInfo('Upload progress--->uploadedSize: ' + uploadedSize + ' ,totalSize--->' + totalSize);
                  this.content = this.getResourceString($r('app.string.Current_upload')) + uploadedSize + 'byte\n'
                  if (uploadedSize >= totalSize) {
                    Log.showInfo('Upload finished');
                    this.content += "\n" + this.getResourceString($r('app.string.Upload_total')) + totalSize + 'byte\n'
                    this.content += "\n" + this.getResourceString($r('app.string.Upload_file_path')) + this.hereCacheDir + "/test2.txt.gz\n"
                  }
                })
                data.uploadTask.on('headerReceive', (headers: object) => {
                  Log.showInfo('Upload--->headerReceive: ' + JSON.stringify(headers));
                })
                data.uploadTask.on('complete', (data: Array<UploadResultCallbackType>) => {
                  this.status = this.getResourceString($r('app.string.Upload_completed'));
                  this.status += "\n" + this.getResourceString($r('app.string.Upload_result')) + data[0].message
                  Log.showInfo('Upload--->complete,data: ' + JSON.stringify(data));
                })
              })
              .catch((error: BusinessError<string>) => {
                this.status = "";
                if (error.data != undefined) {
                  this.content = error.data;
                }
                hilog.info(0x0001, "onError -> Error", this.content + "");
              });
          })


        Button('GZIP Download File')
          .layoutWeight(1)
          .fontSize(18)
          .fontColor(0xCCCCCC)
          .align(Alignment.Center)
          .height('100%')
          .margin({ left: 5 })
          .onClick((event: ClickEvent) => {
            this.status = this.getResourceString($r('app.string.Downloading'))
            this.content = ''
            try {
              Log.showInfo(' abilityContext - cacheDir  = ' + this.hereCacheDir);
              Log.showInfo(' abilityContext - filesDir  = ' + this.hereFilesDir);
              this.downloadfile = this.hereFilesDir + '/server.txt.gz';

              let request: Request = new Request.Builder()
                .download(this.getUrl() + "/downLoadGzipFiles/server.txt.gz")
                .setAbilityContext(this.hereAbilityContext)
                .build();
              this.client.newCall(request)
                .execute()
                .then(async (data: Response) => {
                  data.downloadTask.on('progress', (receivedSize: number, totalSize: number) => {
                    this.content = '\n' + this.getResourceString($r('app.string.Download_size')) + receivedSize + ' byte\n'
                    this.content += '\n' + this.getResourceString($r('app.string.Total_files')) + totalSize + ' byte\n'
                    this.content += "\n" + this.getResourceString($r('app.string.Download_path')) + this.downloadfile + '\n'
                    Log.showInfo('progress--->downloadedSize: ' + receivedSize + ' ,totalSize--->' + totalSize);
                  })
                  data.downloadTask.on('complete', async () => {
                    Log.showInfo("download complete");
                    let appInternalDir = this.hereFilesDir;
                    let dest = appInternalDir + "/helloServer.txt";
                    await gZipUtil.ungZipFile(this.downloadfile, dest);
                    Log.showInfo("gzip Download File test success");
                    let fileID = fs.openSync(dest, fs.OpenMode.READ_WRITE | fs.OpenMode.CREATE)
                    // 获取文件信息
                    let stat = fs.statSync(fileID.fd);
                    let size = stat.size // 文件的大小，以字节为单位
                    let buf = new ArrayBuffer(size);
                    fs.readSync(fileID.fd, buf)
                    let textDecoder = util.TextDecoder.create("utf-8", { ignoreBOM: true });
                    let decodedString = textDecoder.decodeWithStream(new Uint8Array(buf), { stream: false });
                    this.status = this.getResourceString($r('app.string.Download_successful'))
                    this.content += '\n' + this.getResourceString($r('app.string.Download_content')) + decodedString + '\n'
                  })
                })
                .catch((error: BusinessError<string>) => {
                  this.status = this.getResourceString($r('app.string.Request_status')) + error.code.toString();
                  if (error.data != undefined) {
                    this.content = error.data;
                  }
                  hilog.info(0x0001, "onError -> Error", JSON.stringify(error));
                });
            } catch (err) {
              this.status = this.getResourceString($r('app.string.Request_failed'));
              this.content = JSON.stringify(err);
              hilog.error(0x0001, "downloadFile execution failed - errorMsg", err);
            }
          })
      }.margin({ top: 10 }).height(px2vp(60))

      Row() {
        Button('Post GZIP Text')
          .layoutWeight(1)
          .fontSize(18)
          .fontColor(0xCCCCCC)
          .align(Alignment.Center)
          .height('100%')
          .onClick((event: ClickEvent) => {
            this.status = this.getResourceString($r('app.string.Visiting'));
            this.content = ''
            const data = "hello, gzip server!!"

            let compressed: Uint8Array = gZipUtil.gZipString(data);
            let bufferContent = buffer.from(compressed);
            this.content += "\n" + this.getResourceString($r('app.string.Send_original')) + data + "\n";
            this.content += "\n" + this.getResourceString($r('app.string.Send_encoded_text')) + JSON.parse(JSON.stringify(bufferContent))
              .data + "\n";

            let requestBody1: RequestBody = RequestBody.create(data, new Mime.Builder().build().getMime())
            let request: Request = new Request.Builder()
              .url(this.getUrl() + '/postGzipText')
              .post(requestBody1)
              .setGzipBuffer(true)
              .addHeader("Content-Type", "text/plain")
              .addHeader("Accept-Encoding", "gzip")
              .build();

            this.client.newCall(request).enqueue((result: Response) => {
              this.status = '\n' + this.getResourceString($r('app.string.Return_status')) + result.responseCode + '\n';
              if (result.result) {
                this.content += '\n' + this.getResourceString($r('app.string.Return_result')) + result.result + '\n';
                this.content += '\n' + this.getResourceString($r('app.string.Return_header')) + JSON.stringify(result.header) + '\n';
              } else {
                this.content += '\n' + this.getResourceString($r('app.string.Return_result')) + result.result + '\n';
              }
              Log.showInfo("onComplete -> Status : " + result.responseCode);
              Log.showInfo("onComplete -> Content : " + JSON.stringify(result.result));
            }, (error: BusinessError<string>) => {
              this.status = this.getResourceString($r('app.string.Request_status')) + error.code.toString();
              if (error.data != undefined) {
                this.content = error.data;
              }
              hilog.error(0x0001, "onError -> Error", JSON.stringify(error));
            });
          })

        Button('Get GZIP Text')
          .layoutWeight(1)
          .fontSize(18)
          .fontColor(0xCCCCCC)
          .align(Alignment.Center)
          .height('100%')
          .margin({ left: 5 })
          .onClick((event: ClickEvent) => {
            this.status = this.getResourceString($r('app.string.Visiting'))
            this.content = ''
            let request: Request = new Request.Builder()
              .get(this.getUrl() + '/getGzipText')
              .params('key', 'custKey')
              .addHeader("Content-Type", "application/json")
              .addHeader("Accept-Encoding", "gzip")
              .build();
            this.client.newCall(request).enqueue((result: Response) => {
              this.status = '\n' + this.getResourceString($r('app.string.Return_status')) + result.responseCode + '\n';

              if (result.result) {
                this.content = '\n' + this.getResourceString($r('app.string.Return_result')) + result.result + '\n';
                this.content += '\n' + this.getResourceString($r('app.string.Return_header')) + JSON.stringify(result.header) + '\n';
              } else {
                this.content += '\n' + this.getResourceString($r('app.string.Return_result')) + result.result + '\n';
              }
              Log.showInfo("onComplete -> Get GZIP Text Status : " + result.responseCode);
              Log.showInfo("onComplete -> Get GZIP Text Content : " + JSON.stringify(result.result));
            }, (error: BusinessError<string>) => {
              this.status = this.getResourceString($r('app.string.Request_status')) + error.code.toString();
              if (error.data != undefined) {
                this.content = error.data;
              }
              hilog.error(0x0001, "onError -> Error", JSON.stringify(error));
            });
          })
      }.margin({ top: 10 }).height(px2vp(60))

      Scroll(this.scroller) {
        Column() {
          Text(this.status)
            .fontSize(20)
            .fontWeight(FontWeight.Bold)
            .width('100%')
            .textAlign(TextAlign.Start)
          Text(this.content)
            .fontSize(20)
            .width('100%')
            .textAlign(TextAlign.Start)
        }.width('100%')
      }
      .scrollable(ScrollDirection.Vertical)
      .scrollBar(BarState.On)
      .scrollBarColor(Color.Gray)
      .layoutWeight(1)
    }.padding({ left: 5, right: 5 })
  }

  gzipStrAndSaveFile(encodeStr: string, resourcePath: string, gzipPath: string) {
    let fd = fs.openSync(resourcePath, fs.OpenMode.READ_WRITE | fs.OpenMode.CREATE);
    fs.truncateSync(fd.fd);
    fs.writeSync(fd.fd, encodeStr);
    fs.fsyncSync(fd.fd)
    fs.closeSync(fd);
    gZipUtil.gZipFile(resourcePath, gzipPath);
  }

  getUrl() {
    return 'http://' + this.ipInput + ':' + this.portInput
  }
}

interface UploadResultCallbackType {
  path: string;
  responseCode: number;
  message: string;
}